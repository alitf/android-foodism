# Foodism 
Developed by Ali Tafreshi

Main technologies used to build this application :


1. Kotlin
1. MVI + Clean Architecture
1. Repository Pattern
1. Single Activity **(No Fragments)**
1. Dependency Injection **(Hilt)**
1. Pagination 
1. Single Source Of Truth **(SSOT)**
1. Kotlin Coroutines
1. Reactive Streams **(Kotlin Flows/Channels)**
1. Room
1. Ktor
1. Jetpack Compose
1. Splash Screen Apis
1. Async Image Loading **(Coil)**
1. Navigation Components **(Jetpack Compose)**
1. Navigation Animations **(Jetpack Compose)**
1. Material Design Theming
1. Advance Error Handling
1. Advance State Management
1. Google Best Practices
1. Unit Testing
1. Integrated Testing 


# Known issues

1. Because The New Splash Screen Apis is In Alpha  App Loss Its State In Cold Start On Configuration change


# Planned For Future
1. Jetpack Compose Ui Testing


# Google MAD Skills

![Google MAD Skills](https://i.imgur.com/EXhR57V.png)

